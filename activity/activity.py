# s04 Activity


from abc import ABC, abstractclassmethod

class Animal (ABC):

	@abstractclassmethod
	def eat (self, food):
		...

	def make_sound(self):
		...
		

class Dog (Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self.name = name
		self.breed = breed
		self.age = age

	def get_name(self):
		print(f"Name of person: {self.name}")

	def get_breed(self):
		print(f"Name of person: {self.breed}")

	def get_age(self):
		print(f"Age of person: {self.age}")

	def set_name(self, name):
		self.name = name

	def eat (self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print(f"Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self.name}!")


class Cat (Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self.name = name
		self.breed = breed
		self.age = age

	def get_name(self):
		print(f"Name of person: {self.name}")

	def get_breed(self):
		print(f"Name of person: {self.breed}")

	def get_age(self):
		print(f"Age of person: {self.age}")

	def set_name(self, name):
		self.name = name

	def eat (self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f"Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self.name}, Come on!")



dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()


cat1 = Cat("Puss", "Persion", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()